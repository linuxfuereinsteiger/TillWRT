# Multi VPN

Eine gesicherte Multi-VPN Verbindung am heimischen Server, der die Verbindungen sicher durch das Netz leitet.

Verwendete VPN Methoden: 

- OVPN, Tailscale (Wireguard)
- GUI zu administrieren: Webmin
- Firewall: Iptables
- Ubuntu Server 22.XX.amd64

[Ubuntu Server](https://ubuntu.com/download/server) Installationsdauer ca. 1 Stunde

# Installation & Konfiguration

**Statische IP vergeben**

```
sudo nmcli con add type ethernet con-name 'static' ifname eth0 ipv4.method manual ipv4.addresses 192.168.1.2/24 gw4 192.168.1.1
```

```
sudo reboot now
```

```
ssh Nutzer@neue.ip
```

```
sudo apt update && sudo apt upgrade && sudo apt-get full-upgrade -y && sudo apt install wget && sudo apt install network-manager && sudo apt-get install openvpn* easy-rsa
```

```
sudo systemctl enable openvpn

sudo systemctl start openvpn

sudo systemctl status openvpn

```


```
cd /etc/openvpn
```

Auf: [Nordvpn Server](https://nordvpn.com/de/servers/tools/) die passende TCP Server Verbindung herunter laden. z.B..

```
sudo wget https://downloads.nordcdn.com/configs/files/ovpn_tcp/servers/lt14.nordvpn.com.tcp.ovpn && ls
```

Alternativ benötigt Ihr einen VPN Anbieter, der OpenVPN anbietet und das auch für Router bereit stellt.

Nun Ihr seht die Verbindung.ovpn

Passwort speichern.

```
sudo nano /etc/openvpn/nordvpn_auth.txt
```

Dort müsst Ihr folgendes eintragen: Benutzername aus dem Nord Account /OpenVPN Acount Passwort


```
sudo chmod 600 /etc/openvpn/nordvpn_auth.txt
sudo mv lt14.nordvpn.com.tcp.ovpn server.conf
sudo nano server.conf
auth-user-pass /etc/openvpn/nordvpn_auth.txt
sudo systemctl restart openvpn
sudo systemctl enable openvpn
sudo systemctl enable openvpn@server
sudo systemctl start openvpn@server
sudo reboot
```

IP checken

```
wget http://ipinfo.io/ip -qO -
```

## Den Server zum Router fungieren

```
sudo /bin/su -c "echo -e '\n#Enable IP Routing\nnet.ipv4.ip_forward = 1' > /etc/sysctl.conf"
```

```
sudo sysctl -p
```

## Firewall einstellen/ routing

```
sudo iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o tun0 -j ACCEPT
sudo iptables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT

sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -i eth0 -p icmp -j ACCEPT

sudo iptables -A INPUT -i eth0 -p icmp -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --dport 22 -j ACCEPT

sudo iptables -I INPUT -i eth0 -p udp --dport 67:68 --sport 67:68 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 53 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p udp --destination-port 53 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p udp --destination-port 443 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 443 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 80 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 3000 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 10000 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p tcp --destination-port 5353 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p udp --destination-port 5353 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p udp --destination-port 41641 -j ACCEPT
sudo iptables -A INPUT -i eth0 -p udp --destination-port 3478 -j ACCEPT
sudo iptables -P FORWARD DROP
sudo iptables -P INPUT DROP
```

## Tailscale

Tailscale installieren:

```
curl -fsSL https://tailscale.com/install.sh | sh 
```

  
Tailscale konfigurieren:

```
sudo tailscale up echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.d/99-tailscale.conf

sudo sysctl -p /etc/sysctl.d/99-tailscale.conf
ip a
sudo tailscale up --advertise-routes=192.168.0.0/24,10.7.1.2/24

tailscale cert
sudo tailscale up --advertise-exit-node
```

## Samba

samba installieren:

```
sudo apt install samba cifs-utils -y
```

samba konfigurieren:

```
sudo nano /etc/samba/smb.conf
```

```
#server string is the equivalent of the NT Description field
server string = member server (Samba, Ubuntu)
```

  
Zum `/home`\-Verzeichnis wechseln:

```
cd ~
```

## Adguard

Adguard installieren:

```
curl -s -S -L https://raw.githubusercontent.com/AdguardTeam/AdGuardHome/master/scripts/install.sh | sh -s -- -v
```

zum Adguard-Verzeichnis wechseln:

```
cd /opt/AdGuardHome/ 
```

Adguard starten:

```
sudo ./AdGuardHome -s start
```

### Adguard-Befehle

  
**Start AdGuard Home Service**

```
sudo ./AdGuardHome -s start
```

**Stop AdGuard Home Service**

```
sudo ./AdGuardHome -s stop
```

**Restart AdGuard Home Service**

```
sudo ./AdGuardHome -s restart
```

**Install AdGuard Home Service.** This registers it as a Service

```
sudo ./AdGuardHome -s install
```

**Uninstall AdGuard Home Service**

```
sudo ./AdGuardHome -s uninstall
```

**Den Status von Adguard überprüfen:** 

```
sudo ./AdGuardHome -s status
```

Ausgabe:

```
http://ip.des-servers:3000
```

## Webmin

Webmin Installieren:

```
cd ~
```

```
 sudo apt update
```

```
curl -fsSL https://download.webmin.com/jcameron-key.asc | sudo gpg --dearmor -o /usr/share/keyrings/webmin.gpg
```

```
sudo nano /etc/apt/sources.list
```

ganz unten hinzufügen:

```
deb [signed-by=/usr/share/keyrings/webmin.gpg]
http://download.webmin.com/download/repository sarge contrib
```

```
sudo apt update
```

```
sudo apt install webmin -y
```

```
sudo nano /etc/openvpn/server.conf
```

Am Ende des Files folgendes hinzufügen: 

```
push "dhcp-option DNS 192.168.0.1"
```

```
sudo systemctl restart openvpn@server
```

```
sudo apt-get install iptables-persistent
sudo systemctl enable netfilter-persistent
cd ~
```

Webmin ist unter: `ip.des-server:10000` zu erreichen

![Webmin.png](Webmin.png)

![openVPN.png](openVPN.png)

  
Auf dem Linux Rechner:

```
sudo tailscale up --exit-node=hostname --accept-routes --netfilter- mode=on --accept-routes
```

## Quellen

<https://www.makeuseof.com/configure-static-ip-address-settings-ubuntu-22-04/> 

<https://ubuntuhandbook.org/index.php/2022/10/setup-openvpn-ubuntu-2204/> 

<https://github.com/blurrryy/pihole-nordvpn-dhcp>

<https://tailscale.com/kb/1019/subnets/?q=tailscale%20subnet%20>

<https://techviewleo.com/install-and-configure-adguard-home-on-ubuntu/?expand_article=1> 

<https://flathub.org/apps/dev.deedles.Trayscale>

## Authors and acknowledgment

T. Scheibelt [felix.scheibelt@ik.me](mailto:felix.scheibelt@ik.me)

Wiritten by T. Scheibelt (10/2023) for <https://linuxfuereinsteiger.com>

## License

The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
